var costCnt = 0;

$(document).ready(function(){

    $('#btn-start').click(function() {
        $('#btn-start').hide();
        $("#divQuestion1").show();
        addCost(0);
    });

    // q-submit
    $('#q1-submit').click(q1Submit);
    $('#q2-submit').click(q2Submit);

    // reveal hint
    $("#giveHint1-1").click(function(){

        $("#revealHint1-1").show();
        $("#revealHint1-1").css('opacity', '0');
        $("#revealHint1-1").animate({opacity: '1'}, 1200);
        addCost(200);
    });

    $("#giveHint1-2").click(function(){

        $("#revealHint1-2").show();
        $("#revealHint1-2").css('opacity', '0');
        $("#revealHint1-2").animate({opacity: '1'}, 1200);
        addCost(350);
    });

    $("#giveHint2-1").click(function(){

        $("#revealHint2-1").show();
        $("#revealHint2-1").css('opacity', '0');
        $("#revealHint2-1").animate({opacity: '1'}, 1200);
        addCost(300);
    });

    $("#giveHint2-2").click(function(){

        $("#revealHint2-2").show();
        $("#revealHint2-2").css('opacity', '0');
        $("#revealHint2-2").animate({opacity: '1'}, 1200);
        addCost(600);
    });

    $("#send-btn").click(sendMsg);

    // Close
    $("#btn-op1").click(endgame);
    $("#btn-op2").click(endgame);
    $("#btn-op3").click(endgame);
});



function addCost(number){
    costCnt += number;
    $("#subwelcome").text("Total cost: " + costCnt);
}

// function Submit
function q1Submit(){
    const ansSubmit = [
        $("#q1-1").val(),
        $("#q1-2").val()
    ];

    const ans = ["3", "14"];

    var isCorrect = true;
    for (var i = 0; i<2; i++){
        if(ans[i] !== ansSubmit[i]) isCorrect = false;
    }

    if(isCorrect){
        alert("Wow, you got it!");
        $("#code1").show();
        $("#divQuestion2").show();
    } else {
        alert("Error, haha");
    }
}

function q2Submit(){
    const ansSubmit = [
        $("#q2-1").val(),
        $("#q2-2").val(),
        $("#q2-3").val(),
        $("#q2-4").val(),
        $("#q2-5").val(),
        $("#q2-6").val()
    ];

    const ans = ["3", "1", "4", "6", "1", "6"];

    var isCorrect = true;
    for (var i = 0; i<6; i++){
        if(ans[i] !== ansSubmit[i]) isCorrect = false;
    }

    if(isCorrect){
        alert("Wow, you got it!");
        $("#code2").show();
        $("#divQuestion3").show();
    } else {
        alert("Error, haha");
    }
}

function endgame(){
    $("#divQuestion1").hide();
    $("#divQuestion2").hide();
    $("#divQuestion3").hide();

    $("#welcome").text("Goodbye~");
    $("#subwelcome").text("It's the end of this game, and happy White Day!");
    $(".story").show();
    $("#send-container").show();
}

function sendMsg(){
    write($("#send-input").val());
    alert("Message has sent!");
    $("#send-container").hide();
}

var url = "https://script.google.com/macros/s/AKfycbzY6doVFPzmKEFIAyzz88ymBhueqFcjVacHaE0mNj8GqtDOUg/exec";

// Write Data
function write(message) {
    var formdata = new FormData();
    formdata.append("method", "write");
    formdata.append("message", message);

    var requestOptions = {
        method: 'POST',
        mode: 'no-cors',
        body: formdata,
    };

    fetch(url, requestOptions);
}
